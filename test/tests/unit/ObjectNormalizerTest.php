<?php

namespace AmericanReading\ObjectNormalizer\Test;

use AmericanReading\ObjectNormalizer\ObjectNormalizer;
use stdClass;

/**
 * @coversDefaultClass AmericanReading\ObjectNormalizer\ObjectNormalizer
 * @uses AmericanReading\ObjectNormalizer\ObjectNormalizer
 */
class ObjectNormalizerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @covers ::__construct
     */
    public function testCreatesInstances()
    {
        $normalizer = new ObjectNormalizer();
        $this->assertNotNull($normalizer);
    }

    /**
     * @covers ::setRequiredFields
     * @covers ::normalize
     */
    public function testCopiesRequiredFields()
    {
        $normalizer = new ObjectNormalizer();
        $normalizer->setRequiredFields(array("name"));
        $input = (object) array(
            "name" => "Bob"
        );
        $output = $normalizer->normalize($input);
        $this->assertSame("Bob", $output->name);
    }

    /**
     * @covers ::setOptionalFields
     * @covers ::normalize
     */
    public function testCopiesOptionalFields()
    {
        $normalizer = new ObjectNormalizer();
        $normalizer->setOptionalFields(array("name"));
        $input = (object) array(
            "name" => "Bob"
        );
        $output = $normalizer->normalize($input);
        $this->assertSame("Bob", $output->name);
    }

    /**
     * @covers ::setAlias
     * @covers ::normalize
     */
    public function testCopiesValueFromAliasedField()
    {
        $normalizer = new ObjectNormalizer();
        $normalizer->setOptionalFields(array("name"));
        $normalizer->setAlias("name", "title");
        $input = (object) array(
            "title" => "Bob's Burgers"
        );
        $output = $normalizer->normalize($input);
        $this->assertSame("Bob's Burgers", $output->name);
    }

    /**
     * @covers ::setAlias
     * @covers ::normalize
     */
    public function testCopiesValueFromFirstMatchedAlias()
    {
        $normalizer = new ObjectNormalizer();
        $normalizer->setOptionalFields(array("name"));
        $normalizer->setAlias("name", array("not-present", "title", "show"));
        $input = (object) array(
            "show" => "Family Guy",
            "title" => "Bob's Burgers",
        );
        $output = $normalizer->normalize($input);
        $this->assertSame("Bob's Burgers", $output->name);
    }

    /**
     * @covers ::setDefault
     * @covers ::normalize
     */
    public function testProvideDefaultValues()
    {
        $normalizer = new ObjectNormalizer();
        $normalizer->setRequiredFields(array("name", "show", "job"));
        $normalizer->setDefault("name", "Philip. J. Fry");
        $normalizer->setDefault("show", "Futurama");
        $normalizer->setDefault("job", "Delivery boy");
        $expected = (object) array(
            "name" => "Philip. J. Fry",
            "show" => "Futurama",
            "job" => "Delivery boy"
        );
        $output = $normalizer->normalize(new stdClass());
        $this->assertEquals($expected, $output);
    }

    /**
     * @covers ::normalize
     * @expectedException \InvalidArgumentException
     */
    public function testThrowsExceptionForMissingRequiredField()
    {
        $normalizer = new ObjectNormalizer();
        $normalizer->setRequiredFields(array("name"));
        $input = (object) array(
            "character" => "Bob"
        );
        $normalizer->normalize($input);
    }

    /**
     * @covers ::setModifier
     * @covers ::normalize
     */
    public function testModifiesFieldValueUsingCallable()
    {
        $normalizer = new ObjectNormalizer();
        $normalizer->setRequiredFields(array("name"));
        $normalizer->setModifier("name", "strtoupper");
        $input = (object) array(
            "name" => "Bob Belcher"
        );
        $output = $normalizer->normalize($input);
        $this->assertSame("BOB BELCHER", $output->name);
    }

    /**
     * @covers ::normalize
     */
    public function testConvertsInputArrayToObject()
    {
        $normalizer = new ObjectNormalizer();
        $normalizer->setRequiredFields(array("name"));
        $input = array(
            "name" => "Bob"
        );
        $output = $normalizer->normalize($input);
        $this->assertSame("Bob", $output->name);
    }

    /**
     * @covers ::normalize
     * @expectedException \InvalidArgumentException
     */
    public function testThrowsExceptionWhenInputIsNotObjectOrArray()
    {
        $normalizer = new ObjectNormalizer();
        $normalizer->normalize(false);
    }

    /**
     * @covers ::setFinalizer
     * @covers ::normalize
     */
    public function testFinalizerReceivesNormalizedObjectAsParameter()
    {
        $normalizer = new ObjectNormalizer();
        $normalizer->setRequiredFields(array("name"));
        $input = array(
            "name" => "Bob"
        );

        $normalized = $normalizer->normalize($input);
        $finalizerParameter = null;
        $normalizer->setFinalizer(function ($obj) use (&$finalizerParameter) {
            $finalizerParameter = $obj;
            return $obj;
        });
        $normalizer->normalize($input);

        $this->assertEquals($normalized, $finalizerParameter);
    }

    /**
     * @covers ::setFinalizer
     * @covers ::normalize
     */
    public function testReturnsOutputFromFinalizer()
    {
        $normalizer = new ObjectNormalizer();
        $normalizer->setRequiredFields(array("name"));
        $input = array(
            "name" => "Bob"
        );

        $finalized = new stdClass();
        $normalizer->setFinalizer(function ($obj) use ($finalized) {
            return $finalized;
        });
        $output = $normalizer->normalize($input);

        $this->assertSame($finalized, $output);
    }
}
