<?php

namespace AmericanReading\ObjectNormalizer;

use stdClass;

class ObjectNormalizer
{
    private $requiredFields;
    private $optionalFields;
    private $aliases;
    private $defaults;
    private $modifiers;
    private $finalizer;

    public function __construct()
    {
        $this->requiredFields = array();
        $this->optionalFields = array();
        $this->aliases = array();
        $this->defaults = array();
        $this->modifiers = array();
    }

    /**
     * @param object|array $input
     * @return stdClass
     * @throws \InvalidArgumentException $input is the wrong type or is missing a required field.
     */
    public function normalize($input)
    {
        if (is_array($input)) {
            $input = (object) $input;
        }
        if (!is_object($input)) {
            throw new \InvalidArgumentException("Argument must be an object or array.");
        }

        $output = new stdClass();

        $fields = array_merge($this->requiredFields, $this->optionalFields);
        foreach ($fields as $field) {
            if (property_exists($input, $field)) {
                $output->{$field} = $input->{$field};
            } elseif (isset($this->aliases[$field])) {
                foreach ($this->aliases[$field] as $alias) {
                    if (property_exists($input, $alias)) {
                        $output->{$field} = $input->{$alias};
                        break;
                    }
                }
            } elseif (isset($this->defaults[$field])) {
                $output->{$field} = $this->defaults[$field];
            } elseif (in_array($field, $this->requiredFields)) {
                throw new \InvalidArgumentException("Unable to find a value for $field");
            }

            if (isset($output->{$field}, $this->modifiers[$field])) {
                $modifier = $this->modifiers[$field];
                $output->{$field} = $modifier($output->{$field});
            }
        }

        // If there is a finalizer callable, apply it to the output.
        if (isset($this->finalizer)) {
            $finalizer = $this->finalizer;
            $output = $finalizer($output);
        }

        return $output;
    }

    /**
     * @param string[] $fields
     * @return self
     */
    public function setRequiredFields($fields)
    {
        $this->requiredFields = $fields;
        return $this;
    }

    /**
     * @param string[] $fields
     * @return self
     */
    public function setOptionalFields($fields)
    {
        $this->optionalFields = $fields;
        return $this;
    }

    /**
     * @param string $normalizedField
     * @param string|string[] $aliasFields
     * @return self
     */
    public function setAlias($normalizedField, $aliasFields)
    {
        if (!is_array($aliasFields)) {
            $aliasFields = array($aliasFields);
        }
        $this->aliases[$normalizedField] = $aliasFields;
        return $this;
    }

    /**
     * @param string $field
     * @param mixed $value
     * @return self
     */
    public function setDefault($field, $value)
    {
        $this->defaults[$field] = $value;
        return $this;
    }

    /**
     * @param string $field
     * @param callable $callable
     * @return self
     */
    public function setModifier($field, $callable)
    {
        $this->modifiers[$field] = $callable;
        return $this;
    }

    public function setFinalizer($callable)
    {
        $this->finalizer = $callable;
        return $this;
    }
}
